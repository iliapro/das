import './CustomButton.css'

const CustomButton = ({text}) => {
    return (
        <div className={"CustomButton"}>{text}</div>
    );
}

export default CustomButton;
