import './SignUp.css';
import InputField from "../InputField/InputField";
import CustomButton from "../CustomButton/CustomButton";

const SignUp = () => {
    return (
        <div className="SignUp">
            <div style={{flex: "1"}} />
            <InputField type={"text"} placeholder={"Login"} />
            <InputField type={"password"} placeholder={"Password"} />
            <div style={{flex: "1"}} />
            <CustomButton text={"Continue"} />
        </div>
    );
}

export default SignUp;
