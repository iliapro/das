import './InputField.css'

const InputField = ({type, placeholder}) => {
    return (
        <div>
            <input type={type} placeholder={placeholder} className={"InputField"} />
        </div>
    );
}

export default InputField;
