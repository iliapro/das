import './App.css'
import SignUp from '../SingUp/SignUp'

const App = () => {
  const backsNumber = 10
  const back = Math.floor(Math.random() * backsNumber) + 1

  return (
    <div className="App" style={{backgroundImage: `url("./backgrounds/back${back}.png")`}}>
      <SignUp />
    </div>
  );
}

export default App;
